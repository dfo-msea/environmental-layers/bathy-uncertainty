# Create an uncertainty layer for a digital terrain model (DTM) raster

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@gmail.com | tel: 250-363-6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Create an uncertainty layer for a digital terrain model (DTM) than represents the density of input points in a given area.

## Summary
Calculates the density of input points for each raster cell (and a specified neighbourhood) for a DTM raster built from point interpolation.

## Status
Completed

## Contents
Python code that produces the uncertainty layer can be found within the python jupyter notebook 'uncert-bathy.ipynb'. The notebook 'testing-focalstats.ipynb' includes examples of different neighbourhood weighting schemes and testing for different focal statistic (smoothing) methods. The 'messages.log' is a text file that stores all the info and warning messages from the most recent execution of 'uncert-bathy.ipynb'.

## Methods
*  Gets input points from multiple survey shapefiles within the 'shapefile' directory (runs faster when .dbf files are removed first)
*  Calculates the footprint of DTM raster to check that each shapefile boundary are within the raster boundary
*  Exports all input points as a list of coordinates (text file)
*  Calculates point density raster layers based on the number of input points that fall within each raster cell and a neighbourhood
*  The neighbourhood smoothing is completed using a convolution between a weighted neighboorhood array and the point density raster (akin to a weighted focal sum in ArcMap)
*  The square neighbourhood array can vary in size and follows a linear inverse distance weighting scheme (1 in the center and 0 at the edges)
*  Masks the point density raster and the smoothed point density raster by the DTM raster footprint
*  Exports both original and smoothed point density rasters in masked and un-masked versions (creates 4 geotifs) within the 'rasters' directory

### Nearshore 20m bathymetry example
*  Input points were fieldsheet soundings that were combined using are earlier iteration of this code as well as other input points (e.g. sampled multibeam points) that Sarah Davies shared. The attributes were removed from the original input point shapefiles.
*  The smoothing neighbourhood used was 25x25 cells which equates to 500 m in horizontal distance

## Requirements
Python 3.7.7 and Jupyter Notebook 6.0.3

## Caveats
The point density layer is only a good measure of the DTM uncertainty if all the input points used to build the DTM are used to calculate the point density raster. In the Nearshore 20m bathymetry example, the sampled multibeam input points were missing from HG. The selection of the neighbourhood size can effect the result and should be selected with consideration of the DTM resolution and the interpolation method used (e.g., whether point interpolation was constrained by some distance).
